## Instalace
**iPrima API sandbox** je Symfony aplikace (verze 3.3); zdrojový kód obsahuje již všechny závislosti a konfigurace (není nutné spouštět `composer update` a vytvářet konfigurační soubor). Aplikaci lze spustit dvěma způsoby:
* **S využitím vestavěného PHP web serveru**
  * Jděte do adresáře projektu a spusťte příkaz:
    `$ php bin/console server:run`
  * V prohlížeči zadejte http://localhost:8000/ - zobrazí se homepage aplikace.
* **S použitím plného web serveru (Apache, Nginx)**
  * Následujte postup na http://symfony.com/doc/current/setup/web_server_configuration.html podle konkrétního web serveru.
  * Pro vývojové prostředí je nutné nasměrovat request na _app_dev.php_ místo _app.php_. Např. pro Apache to znamená změnit 
  `RewriteRule ^(.*)$ app.php [QSA,L]`
  na
  `RewriteRule ^(.*)$ app_dev.php [QSA,L]`
  
## Použití pro GraphQL
* Vstupní bod pro GraphQL dotazy je na cestě _/graphql_, např. `localhost:8000/graphql?query={searchResult{title}}`.
* Na cestě _/graphql/explorer_ je GraphiQl editor pro testovaní dotazů na GraphQL server.
