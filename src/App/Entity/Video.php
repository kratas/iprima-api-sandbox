<?php

namespace App\Entity;

class Video {

	private $id;
	private $title;
	private $broadcastDate;
	private $length;
	private $genres;
	private $year;
	private $countries;


	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}


	public function setId($id) {
		$this->id = $id;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}


	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	
	/**
	 * @return string
	 */
	public function getBroadcastDate() {
		return $this->broadcastDate;
	}


	public function setBroadcastDate($broadcastDate) {
		$this->broadcastDate = $broadcastDate;
		return $this;
	}


	/**
	 * @return int
	 */
	public function getLength() {
		return $this->length;
	}


	public function setLength($length) {
		$this->length = $length;
		return $this;
	}


	/**
	 * @return array
	 */
	public function getGenres() {
		return $this->genres;
	}


	public function setGenres($genres) {
		$this->genres = $genres;
		return $this;
	}


	/**
	 * @return int
	 */
	public function getYear() {
		return $this->year;
	}


	public function setYear($year) {
		$this->year = $year;
		return $this;
	}


	/**
	 * @return array
	 */
	public function getCountries() {
		return $this->countries;
	}


	public function setCountries($countries) {
		$this->countries = $countries;
		return $this;
	}
}
