<?php


namespace App\GraphQL\Schema\Type\ListType;

use App\GraphQL\Schema\Type\InterfaceType\NodeInterfaceType;
use Youshido\GraphQL\Type\ListType\AbstractListType;

class NodeListType extends AbstractListType
{
    public function getItemType()
    {
        return new NodeInterfaceType();
    }
}