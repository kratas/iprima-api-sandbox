<?php

namespace App\GraphQL\Schema\Type\InterfaceType;

use App\GraphQL\Schema\Field\ImageField;
use App\GraphQL\Schema\Type\Object\VideoNodeType;
use Youshido\GraphQL\Field\Field;
use Youshido\GraphQL\Type\InterfaceType\AbstractInterfaceType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

class NodeInterfaceType extends AbstractInterfaceType
{
    /**
     * @inheritdoc
     */
    public function build($config)
    {
        $config->addFields([
            'nid' => new NonNullType(new IdType()),
            'title' => new NonNullType(new StringType()),
            new Field([
                'name' => 'teaser',
                'type' => new StringType(),
                'resolve' => ['@App\Service\GraphQL\Resolver', 'getTeaser'],
            ]),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function resolveType($object)
    {
        return new VideoNodeType();
    }
}