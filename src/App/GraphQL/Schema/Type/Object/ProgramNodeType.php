<?php

namespace App\GraphQL\Schema\Type\Object;

use App\GraphQL\Schema\Type\InterfaceType\NodeInterfaceType;
use Youshido\GraphQL\Field\Field;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class ProgramNodeType extends AbstractObjectType
{
    /**
     * @inheritdoc
     */
    public function build($config)
    {
        $config->applyInterface(new NodeInterfaceType());
        $config->addFields([
            new Field([
                'name' => 'id',
                'type' => new NonNullType(new IdType()),
            ]),
            new Field([
                'name' => 'genres',
                'type' => new ListType(new StringType()),
            ]),
        ]);

    }

    public function getInterfaces()
    {
        return [new NodeInterfaceType()];
    }
}