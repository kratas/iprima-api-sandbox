<?php

namespace App\GraphQL\Schema\Type\Object;

use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;

class SearchResultType extends AbstractObjectType
{
    const FIELD_EPISODES = 'episodes';
    const FIELD_MOVIES = 'movies';
    const FIELD_PROGRAMS = 'programs';

    /**
     * @inheritdoc
     */
    public function build($config)
    {
        $config->addFields([
            self::FIELD_EPISODES => new ListType(new VideoNodeType()),
            self::FIELD_MOVIES => new ListType(new VideoNodeType()),
            self::FIELD_PROGRAMS => new ListType(new ProgramNodeType()),
        ]);
    }
}