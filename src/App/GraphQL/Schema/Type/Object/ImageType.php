<?php

namespace App\GraphQL\Schema\Type\Object;

use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class ImageType extends AbstractObjectType
{
    const IMAGE_STYLES = [
        'medium',
        'thumbnail',
        'landscapeExtraLarge',
        'landscapeLarge',
        'landscapeMedium',
        'landscapeSmall',
        'landscapeTiny',
        'portraitBig',
        'portraitMedium',
        'portraitSmall',
        'squareBig',
        'squareMedium',
        'squareMediumSmaller',
        'squareSmall',
        'squareMini',
        'squareTiny',
    ];

    /**
     * @inheritdoc
     */
    public function build($config)
    {
        foreach (self::IMAGE_STYLES as $style) {
            $config->addField($style, new StringType());
        }
    }
}