<?php

namespace App\GraphQL\Schema\Type\Object;

use App\GraphQL\Schema\Type\InterfaceType\NodeInterfaceType;
use Youshido\GraphQL\Field\Field;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

class VideoNodeType extends AbstractObjectType
{
    /**
     * @inheritdoc
     */
    public function build($config)
    {
        $config->applyInterface(new NodeInterfaceType());
        $config->addFields([
            new Field([
                'name' => 'id',
                'type' => new NonNullType(new IdType()),
            ]),
            new Field([
                'name' => 'year',
                'type' => new IntType(),
            ]),
            new Field([
                'name' => 'title',
                'type' => new NonNullType(new StringType()),
            ]),
            new Field([
                'name' => 'broadcastDate',
                'type' => new StringType(),
            ]),
            new Field([
                'name' => 'length',
                'type' => new IntType(),
            ]),
            new Field([
                'name' => 'countries',
                'type' => new ListType(new StringType()),
            ]),
            new Field([
                'name' => 'genres',
                'type' => new ListType(new StringType()),
            ]),
        ]);
    }

    public function getInterfaces()
    {
        return [new NodeInterfaceType()];
    }
}