<?php

namespace App\GraphQL\Schema\Field;

use \App\GraphQL\Schema\Type\Object\VideoNodeType;
use \Youshido\GraphQL\Config\Field\FieldConfig;
use \Youshido\GraphQL\Execution\ResolveInfo;
use \Youshido\GraphQL\Type\AbstractType;
use \Youshido\GraphQL\Type\NonNullType;
use \Youshido\GraphQL\Type\Object\AbstractObjectType;
use \Youshido\GraphQL\Type\Scalar\IdType;
use \Youshido\GraphQLBundle\Field\AbstractContainerAwareField;

class VideoByIdField extends AbstractContainerAwareField {


	public function build(FieldConfig $config) {
		$config->addArguments([
			'id' => new NonNullType(new IdType())
		]);
	}


	public function resolve($value, array $args, ResolveInfo $info) {
		return $this->container->get('App\Service\GraphQL\VideoResolver')->findById($args['id']);
	}


	/**
	 * @return AbstractObjectType|AbstractType
	 */
	public function getType() {
		return new VideoNodeType();
	}
}
