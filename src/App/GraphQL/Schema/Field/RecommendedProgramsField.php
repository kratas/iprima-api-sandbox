<?php

namespace App\GraphQL\Schema\Field;

use App\GraphQL\Schema\Type\Object\ProgramNodeType;
use App\Service\GraphQL\ImageFieldProcessor;
use App\Service\Solr\IPrimaClient;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;

class RecommendedProgramsField extends AbstractContainerAwareField
{
    public function getType()
    {
        return new ListType(new ProgramNodeType());
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('channel', new StringType());
        $config->addArgument('count', new IntType());
        $config->addArgument('offset', new IntType());
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        $out = [];
        /** @var IPrimaClient $client */
        $client = $this->container->get('App\Service\Solr\IPrimaClient');
        $query = $client->createSelectQuery();
        $query->createFilterQuery('type')->setQuery('type:program');
        $query->setSorts(['sort_title' => 'asc']);
        if (isset($args['count']) && $args['count']) {
            $query->setRows($args['count']);
        }
        if (isset($args['offset']) && $args['offset']) {
            $query->setStart($args['offset']);
        }
        $result = $client->select($query);
        $data = $result->getData();
        if (isset($data['response']['docs'])) {
            /** @var ImageFieldProcessor $imageFieldProcessor */
            $imageFieldProcessor = $this->container->get('App\Service\GraphQL\ImageFieldProcessor');

            foreach ($data['response']['docs'] as $doc) {
                $out[$doc['nid']] = $doc;
                if ($info->getFieldAST('image') && isset($doc['im_field_image_primary'])) {
                    $imageFieldProcessor->process($out, $doc['im_field_image_primary'], array($doc['nid']));
                }
            }
            $imageFieldProcessor->addNotFoundImages($out);
        }

        return $out;
    }
}