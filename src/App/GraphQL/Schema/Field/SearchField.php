<?php

namespace App\GraphQL\Schema\Field;

use App\GraphQL\Schema\Type\Object\SearchResultType;
use App\Service\GraphQL\ImageFieldProcessor;
use App\Service\Solr\IPrimaClient;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Parser\Ast\Query;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;

class SearchField extends AbstractContainerAwareField
{
    const NUM_RESULTS = 6;

    /**
     * Resolved field output
     * @var array
     */
    private $output;

    /**
     * @var ImageFieldProcessor
     */
    private $imageFieldProcessor;

    private $groupQueries = [];

    public function getType()
    {
        return new SearchResultType();
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('query', new NonNullType(new StringType()));
        $config->addArgument('boostChannel', new StringType());
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        $result = $this->search($args['query'], $info);
        $data = $result->getData();

        $this->imageFieldProcessor = $this->container->get('App\Service\GraphQL\ImageFieldProcessor');

        if (isset($data['grouped']) && ($groups = $data['grouped'])) {
            $groupKeys = array_flip($this->groupQueries);
            foreach ($groups as $groupQuery => $groupResult) {
                $docs = $groupResult['doclist']['docs'] ?? array();
                $groupKey = $groupKeys[$groupQuery];
                $field = $info->getFieldAST($groupKey);
                $fetchImage = $field->hasField('image');
                foreach ($docs as $doc) {
                    $this->processSearchDocument($doc, $groupKey, $fetchImage);
                }
            }
        }

        $this->imageFieldProcessor->addNotFoundImages($this->output);
        return $this->output;
    }

    /**
     * @param string $queryString
     * @param ResolveInfo $info
     * @return \Solarium\QueryType\Select\Result\Result
     */
    private function search(string $queryString, ResolveInfo $info)
    {
        $fields = $info->getFieldASTList();
        /** @var Query $field */
        foreach ($fields as $field) {
            $fieldName = $field->getName();
            if (SearchResultType::FIELD_PROGRAMS === $fieldName) {
                $this->groupQueries[$fieldName] = 'type:program AND bs_has_episode:true';
            }
            elseif (SearchResultType::FIELD_EPISODES === $fieldName) {
                $this->groupQueries[$fieldName] = 'type:video AND ss_field_category:EPISODE';
            }
            elseif (SearchResultType::FIELD_MOVIES === $fieldName) {
                $this->groupQueries[$fieldName] = 'type:video AND ss_field_category:MOVIE';
            }
        }

        /** @var IPrimaClient $client */
        $client = $this->container->get('App\Service\Solr\IPrimaClient');
        $query = $client->createSelectQuery();
        $query->setQuery($query->getHelper()->escapeTerm($queryString));

        $disMax = $query->getDisMax();
        $disMax->setBoostFunctions('is_search_boost^10 recip(if(exists(ds_time_start),ms(ds_time_start,NOW/DAY),ms(NOW/DAY,ds_cck_field_date_publication)),3.16e-11,1,1)^1000');
        $queryFields = 'ngram_title';
        if (mb_strlen($queryString) >= 5) {
            $queryFields .= ' OR title';
        }
        $disMax->setQueryFields($queryFields);

        $grouping = $query->getGrouping();
        $grouping->setQueries(array_values($this->groupQueries));
        $grouping->setLimit(self::NUM_RESULTS);

        return $client->select($query);
    }

    /**
     * Process a document from the search result and add it to the field output.
     *
     * @param array $doc
     * @param string $group
     * @param bool $fetchImage
     */
    private function processSearchDocument(array $doc, string $group, bool $fetchImage)
    {
        $nid = $doc['nid'];
        $this->output[$group][$nid] = $doc;

        $imageValue = $doc['im_field_image_primary'] ?? false;
        if ($fetchImage && !empty($imageValue)) {
            $this->imageFieldProcessor->process($this->output, $doc['im_field_image_primary'], array($group, $nid));
        }
    }
}