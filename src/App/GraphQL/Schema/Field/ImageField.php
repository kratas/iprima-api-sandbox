<?php

namespace App\GraphQL\Schema\Field;

use App\Document\ImageStyles;
use App\GraphQL\Schema\Type\Object\ImageType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;

class ImageField extends AbstractContainerAwareField
{
    public function getType()
    {
        return new ImageType();
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        $out = array();
        $image = $value['image'] ?? false;
        if ($image && $image instanceof ImageStyles) {
            $fields = $info->getFieldASTList();
            foreach ($fields as $field) {
                $fieldName = $field->getName();
                $getter = 'get' . ucfirst($fieldName);
                if (method_exists($image, $getter)) {
                    $out[$fieldName] = call_user_func(array($image, $getter));
                }
            }
        }
        return $out;
    }
}