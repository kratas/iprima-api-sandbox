<?php

namespace App\GraphQL;

use App\GraphQL\Schema\Field\ProgramByIdField;
use App\GraphQL\Schema\Field\RecommendedProgramsField;
use App\GraphQL\Schema\Field\SearchField;
use App\GraphQL\Schema\Field\VideoByIdField;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Youshido\GraphQL\Config\Schema\SchemaConfig;
use Youshido\GraphQL\Schema\AbstractSchema;

class Schema extends AbstractSchema
{
    /**
     * @var ManagerRegistry
     */
    private $mongoDbRegistry;

    public function __construct()
    {
        parent::__construct();
    }

    public function build(SchemaConfig $config)
    {
        $config->getQuery()->addFields([
            new SearchField(),
            new RecommendedProgramsField(),
            new VideoByIdField(),
            new ProgramByIdField(),
        ]);
    }
}

