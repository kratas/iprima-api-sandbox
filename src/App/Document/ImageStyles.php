<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class ImageStyles
{
    /**
     * @MongoDB\Id(strategy="NONE", type="integer")
     */
    protected $nid;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $medium;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $thumbnail;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landscapeExtraLarge;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landscapeLarge;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landscapeMedium;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landscapeSmall;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $landscapeTiny;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $portraitBig;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $portraitMedium;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $portraitSmall;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareBig;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareMedium;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareMediumSmaller;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareSmall;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareMini;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $squareTiny;

    /**
     * Check if any of requested fields has null value.
     *
     * @param array $fields
     * @return bool
     */
    public function hasNullFields(array $fields) {
        foreach ($fields as $field) {
            if (!isset($this->{$field})) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set nid
     *
     * @param integer $nid
     * @return $this
     */
    public function setNid($nid)
    {
        $this->nid = $nid;
        return $this;
    }

    /**
     * Get nid
     *
     * @return integer $nid
     */
    public function getNid()
    {
        return $this->nid;
    }

    /**
     * Set medium
     *
     * @param string $medium
     * @return $this
     */
    public function setMedium($medium)
    {
        $this->medium = $medium;
        return $this;
    }

    /**
     * Get medium
     *
     * @return string $medium
     */
    public function getMedium()
    {
        return $this->medium;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return $this
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string $thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set landscapeLarge
     *
     * @param string $landscapeLarge
     * @return $this
     */
    public function setLandscapeLarge($landscapeLarge)
    {
        $this->landscapeLarge = $landscapeLarge;
        return $this;
    }

    /**
     * Get landscapeLarge
     *
     * @return string $landscapeLarge
     */
    public function getLandscapeLarge()
    {
        return $this->landscapeLarge;
    }

    /**
     * Set landscapeExtraLarge
     *
     * @param string $landscapeExtraLarge
     * @return $this
     */
    public function setLandscapeExtraLarge($landscapeExtraLarge)
    {
        $this->landscapeExtraLarge = $landscapeExtraLarge;
        return $this;
    }

    /**
     * Get landscapeExtraLarge
     *
     * @return string $landscapeExtraLarge
     */
    public function getLandscapeExtraLarge()
    {
        return $this->landscapeExtraLarge;
    }

    /**
     * Set landscapeMedium
     *
     * @param string $landscapeMedium
     * @return $this
     */
    public function setLandscapeMedium($landscapeMedium)
    {
        $this->landscapeMedium = $landscapeMedium;
        return $this;
    }

    /**
     * Get landscapeMedium
     *
     * @return string $landscapeMedium
     */
    public function getLandscapeMedium()
    {
        return $this->landscapeMedium;
    }

    /**
     * Set landscapeSmall
     *
     * @param string $landscapeSmall
     * @return $this
     */
    public function setLandscapeSmall($landscapeSmall)
    {
        $this->landscapeSmall = $landscapeSmall;
        return $this;
    }

    /**
     * Get landscapeSmall
     *
     * @return string $landscapeSmall
     */
    public function getLandscapeSmall()
    {
        return $this->landscapeSmall;
    }

    /**
     * Set landscapeTiny
     *
     * @param string $landscapeTiny
     * @return $this
     */
    public function setLandscapeTiny($landscapeTiny)
    {
        $this->landscapeTiny = $landscapeTiny;
        return $this;
    }

    /**
     * Get landscapeTiny
     *
     * @return string $landscapeTiny
     */
    public function getLandscapeTiny()
    {
        return $this->landscapeTiny;
    }

    /**
     * Set portraitBig
     *
     * @param string $portraitBig
     * @return $this
     */
    public function setPortraitBig($portraitBig)
    {
        $this->portraitBig = $portraitBig;
        return $this;
    }

    /**
     * Get portraitBig
     *
     * @return string $portraitBig
     */
    public function getPortraitBig()
    {
        return $this->portraitBig;
    }

    /**
     * Set portraitMedium
     *
     * @param string $portraitMedium
     * @return $this
     */
    public function setPortraitMedium($portraitMedium)
    {
        $this->portraitMedium = $portraitMedium;
        return $this;
    }

    /**
     * Get portraitMedium
     *
     * @return string $portraitMedium
     */
    public function getPortraitMedium()
    {
        return $this->portraitMedium;
    }

    /**
     * Set portraitSmall
     *
     * @param string $portraitSmall
     * @return $this
     */
    public function setPortraitSmall($portraitSmall)
    {
        $this->portraitSmall = $portraitSmall;
        return $this;
    }

    /**
     * Get portraitSmall
     *
     * @return string $portraitSmall
     */
    public function getPortraitSmall()
    {
        return $this->portraitSmall;
    }

    /**
     * Set squareBig
     *
     * @param string $squareBig
     * @return $this
     */
    public function setSquareBig($squareBig)
    {
        $this->squareBig = $squareBig;
        return $this;
    }

    /**
     * Get squareBig
     *
     * @return string $squareBig
     */
    public function getSquareBig()
    {
        return $this->squareBig;
    }

    /**
     * Set squareMedium
     *
     * @param string $squareMedium
     * @return $this
     */
    public function setSquareMedium($squareMedium)
    {
        $this->squareMedium = $squareMedium;
        return $this;
    }

    /**
     * Get squareMedium
     *
     * @return string $squareMedium
     */
    public function getSquareMedium()
    {
        return $this->squareMedium;
    }

    /**
     * Set squareMediumSmaller
     *
     * @param string $squareMediumSmaller
     * @return $this
     */
    public function setSquareMediumSmaller($squareMediumSmaller)
    {
        $this->squareMediumSmaller = $squareMediumSmaller;
        return $this;
    }

    /**
     * Get squareMediumSmaller
     *
     * @return string $squareMediumSmaller
     */
    public function getSquareMediumSmaller()
    {
        return $this->squareMediumSmaller;
    }

    /**
     * Set squareSmall
     *
     * @param string $squareSmall
     * @return $this
     */
    public function setSquareSmall($squareSmall)
    {
        $this->squareSmall = $squareSmall;
        return $this;
    }

    /**
     * Get squareSmall
     *
     * @return string $squareSmall
     */
    public function getSquareSmall()
    {
        return $this->squareSmall;
    }

    /**
     * Set squareMini
     *
     * @param string $squareMini
     * @return $this
     */
    public function setSquareMini($squareMini)
    {
        $this->squareMini = $squareMini;
        return $this;
    }

    /**
     * Get squareMini
     *
     * @return string $squareMini
     */
    public function getSquareMini()
    {
        return $this->squareMini;
    }

    /**
     * Set squareTiny
     *
     * @param string $squareTiny
     * @return $this
     */
    public function setSquareTiny($squareTiny)
    {
        $this->squareTiny = $squareTiny;
        return $this;
    }

    /**
     * Get squareTiny
     *
     * @return string $squareTiny
     */
    public function getSquareTiny()
    {
        return $this->squareTiny;
    }
}
