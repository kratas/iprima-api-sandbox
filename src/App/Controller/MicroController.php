<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class MicroController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return new Response('');
    }
}
