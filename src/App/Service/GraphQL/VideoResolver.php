<?php

namespace App\Service\GraphQL;

use \App\Entity\Video;
use \App\Repository\VideoRepository;

class VideoResolver {

	/**
	 * @param int $id
	 * @return Video
	 */
	public function findById($id) {
		return (new VideoRepository())->findById($id);
	}
}
