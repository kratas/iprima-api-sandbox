<?php

namespace App\Service\GraphQL;

use Youshido\GraphQL\Execution\ResolveInfo;

class Resolver
{
    public function getBroadcastDate($value, array $args, ResolveInfo $info)
    {
        return $value['ds_field_video_date'] ?? '';
    }

    public function getTeaser($value, array $args, ResolveInfo $info)
    {
        return $value['ts_cck_field_teaser'] ?? '';
    }

    public function getVideoLength($value, array $args, ResolveInfo $info)
    {
        // @todo: Get real data from search result document.
        return 30;
    }
}