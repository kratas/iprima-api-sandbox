<?php

namespace App\Service\GraphQL;

use App\Document\ImageStyles;
use App\GraphQL\Schema\Type\Object\ImageType;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class ImageFieldProcessor
{
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $imageCropsEndpoint;
    private $notFoundImages = [];

    public function __construct(LoggerInterface $logger, $imageCropsEndpoint)
    {
//        $this->managerRegistry = $mr;
        $this->logger = $logger;
        $this->imageCropsEndpoint = $imageCropsEndpoint;
    }

    public function process(array &$output, $imageNid, array $arrayPath = array())
    {
        if ($imageNid) {
            $imageNid = is_array($imageNid) ? $imageNid[0] : $imageNid;
            array_push($arrayPath, 'image');
            if ($image = $this->getImageById($imageNid)) {
                $value = $this->setNestedArrayValue($arrayPath, $image);
                $output = array_replace_recursive($output, $value);
            }
            else {
                $this->notFoundImages[$imageNid] = $arrayPath;
            }
        }
    }

    public function addNotFoundImages(&$output)
    {
        if (!empty($this->notFoundImages)) {
            $images = $this->fetchRemoteImages(array_keys($this->notFoundImages));
            foreach ($images as $imageNid => $imageStylesObject) {
                $arrayPath = $this->notFoundImages[$imageNid];
                $value = $this->setNestedArrayValue($arrayPath, $imageStylesObject);
                $output = array_replace_recursive($output, $value);
            }
        }
    }

    private function getImageById($nid)
    {
        /** @var ImageStyles $image */
        $image = $this->managerRegistry
            ->getRepository('AppBundle:ImageStyles')
            ->find($nid);

        if ($image && !$image->hasNullFields(ImageType::IMAGE_STYLES)) {
            return $image;
        }
        return false;
    }

    /**
     * @param array $imageNids
     * @return array
     */
    private function fetchRemoteImages(array $imageNids)
    {
        $images = [];
        try {
            $imageStyles = $this->retrieveRemoteImageStyles($imageNids);
            foreach ($imageStyles as $imageNid => $styles) {
                $image = $this->persistImageStyles($imageNid, $styles);
                $images[$imageNid] = $image;
            }
        } catch (\Exception $e) {
            $this->logger->error('Exception caught', ['exception' => $e]);
        }
        return $images;
    }

    /**
     * Retrieve image styles from remote resource.
     *
     * @param array $imageNids
     * @return array
     * @throws \Exception
     */
    private function retrieveRemoteImageStyles(array $imageNids)
    {
        $imageStyles = array_map(function ($style) {
            $camelCaseConverter = new CamelCaseToSnakeCaseNameConverter();
            return $camelCaseConverter->normalize($style);
        }, ImageType::IMAGE_STYLES);

        $data = array(
            'nids' => $imageNids,
            'image_styles' => $imageStyles,
        );

        $url = $this->imageCropsEndpoint;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            throw new \Exception(sprintf('cURL error (%d) %s for %s', curl_errno($ch), curl_error($ch), $url));
        }
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status != 200) {
            throw new \Exception(sprintf('Received status %d from %s', $status, $url));
        }

        if ($result) {
            return json_decode($result, true);
        }
        return array();
    }

    /**
     * Persist image styles in MongoDB.
     *
     * @param int $nid
     * @param array $styles
     * @return ImageStyles
     */
    private function persistImageStyles($nid, array $styles)
    {
        $imageStyles = new ImageStyles();
        $imageStyles->setNid($nid);
        foreach ($styles as $style => $url) {
            $camelCaseConverter = new CamelCaseToSnakeCaseNameConverter();
            $style = $camelCaseConverter->denormalize($style);
            $setter = 'set' . ucfirst($style);
            if (method_exists($imageStyles, $setter)) {
                call_user_func(array($imageStyles, $setter), $url);
            }
        }

        /** @var DocumentManager $dm */
        $dm = $this->managerRegistry->getManager();
        $dm->persist($imageStyles);
        $dm->flush();

        return $imageStyles;
    }

    private function setNestedArrayValue($path, $value)
    {
        $key = array_shift($path);
        $array[$key] = empty($path) ? $value : $this->setNestedArrayValue($path, $value);
        return $array;
    }
}
