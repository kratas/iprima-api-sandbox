<?php

namespace App\Service\Solr;

use Solarium\Core\Client\Client;
use Solarium\QueryType\Select\Query\Query;

class IPrimaClient
{
    const DEFAULT_ROWS_COUNT = 10;

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $fields
     * @return Query
     */
    public function createSelectQuery(array $fields = [])
    {
        $query = $this->client->createSelect();
        $query->createFilterQuery('drupal')->setQuery('is_drupal:7');
        $query->createFilterQuery('status')->setQuery('status:true');
        $query->setRows(self::DEFAULT_ROWS_COUNT);
        if (!empty($fields)) {
            $query->setFields($fields);
        }
        return $query;
    }

    /**
     * @param Query $query
     * @return \Solarium\QueryType\Select\Result\Result
     */
    public function select(Query $query)
    {
        return $this->client->select($query);
    }
}