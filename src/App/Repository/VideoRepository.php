<?php

namespace App\Repository;

class VideoRepository {


	/**
	 * @return Video
	 */
	public function findById($id) {

		/*@TODO*/
		$x = new \App\Entity\Video();
		$x->setId($id);
		$x->setBroadcastDate('2017-05-08 11:00:00');
		$x->setLength(30);
		$x->setCountries(['Russia','USA']);
		$x->setGenres('Vaření','Seriál');
		$x->setTitle('Vaříme s velmocemi III.');
		$x->setYear(2017);
		return $x;
	}
}
